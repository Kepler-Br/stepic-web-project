def app(environ, start_response):
    start_response("200 OK", [("Content-Type", "text/plain")])
    query_list  = environ["QUERY_STRING"].replace('&', '\n')
    return [query_list]
